package pl.poznan.put.spio;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* Klasa stack opisująca stos, oznaczona adnotacją @Slf4j (pozwala na logowanie) oraz @Service (oznacza serwis
 wykrywalny przez framework Spring, dzięki czemu instancje klasy stack nie muszą być tworzone ręcznie -
 patrz StackController).*/
@Slf4j
@Service
public class Stack {

    // deklaracje potrzebnych pól
    private final int MAX = 100;
    private List<Object> stack;

    private int pointer;

    // konstruktor, który odpowiada za utworzenie instancji klasy
    public Stack() {
        stack = new ArrayList<>(MAX);
    }

    // metoda pozwalająca pobrać zawartość stosu
    public Collection<Object> getCollection() {
        return stack;
    }

    // metoda zdejmuąca element ze stosu
    public Object pop() {
        log.info("pop {}", pointer);
        return stack.get(--pointer);
    }

    // metoda odkładająca element na wierzch stosu
    public void push(Object o) {
        log.info("push {} {}", pointer, o);
        stack.add(pointer, o);
        pointer++;
    }

    // metoda czyszcząca stan stosu
    public void reset() {
        pointer = 0;
    }

    // metoda pobierająca aktualny rozmiar stosu
    public int size() {
        return pointer;
    }

}
