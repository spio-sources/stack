package pl.poznan.put.spio;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// Klasa testująca
public class StackTest {

    // Klasa testowana
    private Stack stack;

    // Metoda z adnotacją @BeforeEach pozwala ustawić pewien stan początkowy,
    // tutaj tworzy instancję Stack oraz wrzuca jeden element
    @BeforeEach
    public void init() {
        stack = new Stack();
        stack.push("aaa");
    }

    // Metoda z adnotacją @Test jest testem jednostkowym
    @Test
    public void shouldPushElementOnStack() {
        // when
        stack.push("bbb");

        // elementy assert* pozwalają tworzyć asercje/zapewnienia,
        // po których spełnieniu możemy stwierdzić że test zakończył się bez błędu
        // then
        assertAll("check stack conditions",
                () -> assertEquals(2, stack.size()),
                () -> assertEquals("bbb", stack.pop()));
    }

    @Test
    public void shouldPopElementFromStack() {
        // when
        final Object result = stack.pop();

        // then
        assertEquals("aaa", result);
    }

    @Test
    public void shouldGetAllElements() {
        // when
        final Collection<Object> result = stack.getCollection();

        // then
        assertArrayEquals(new Object[]{"aaa"}, result.toArray());
    }

    @Test
    public void shouldCheckStackSize() {
        // when
        int size = stack.size();

        // then
        assertEquals(1, size);
    }

}
